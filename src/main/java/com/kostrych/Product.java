package com.kostrych;

public abstract class Product {
    private String nameOfProduct;
    private int productPrice;

    public Product(String nameOfProduct, int productPrice) {
        this.nameOfProduct = nameOfProduct;
        this.productPrice = productPrice;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

}
