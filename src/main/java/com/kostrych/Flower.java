package com.kostrych;

public class Flower extends Product {
    private String flowerColor;

    public Flower(String nameOfProduct, int productPrice, String flowerColor) {
        super(nameOfProduct, productPrice);
        this.flowerColor = flowerColor;
    }

    public String getFlowerColor() {
        return flowerColor;
    }

    @Override
    public String toString() {
        return "Flower{" + "nameOfFlowers: " + this.getNameOfProduct() + "\t\\\t"
                +"flowerColor=" + flowerColor + "\t\\\t"
                + "flowerPrice=" + this.getProductPrice()
                + "}";
    }
}
