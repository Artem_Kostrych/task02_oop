package com.kostrych;

import java.util.LinkedList;
import java.util.List;

public class FlowerShop {
    private List<Employee> employees;
    private List<Product> products;
    private boolean open;

    public FlowerShop(List<Employee> employees, List<Product> products, boolean open) {
        this.employees = employees;
        this.products = products;
        this.open = open;
    }

    public static void main(String[] args) {
        FlowerShop flowerShop = new FlowerShop(new LinkedList<Employee>(), new LinkedList<Product>(), true);
        flowerShop.employees = new LinkedList<Employee>();
        flowerShop.products = new LinkedList<Product>();

        Employee worker = new Employee("Artem Kostrych");
        flowerShop.employees.add(worker);

        flowerShop.products.add(new Flower("Rose",
                25, "White"));
        flowerShop.products.add(new Flower("Tulip",
                15, "Red"));
        flowerShop.products.add(new Flower("Narcissus",
                10, "Blue"));

        Bouquet bouquet = flowerShop.employees.get(0).makeBouquet(flowerShop);
        System.out.println(bouquet);

        bouquet.setBouquet(flowerShop.employees.get(0).sortFlowers(bouquet));
        System.out.println(bouquet);

        System.out.println(flowerShop.employees.get(0)
                .findWithLambda(flowerShop.products, flowerShop.employees));
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public String toString() {
        return "FlowerShop{"
                + "employees=" + employees
                + ", products=" + products
                + ", open=" + open
                + '}';
    }
}
