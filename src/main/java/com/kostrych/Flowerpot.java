package com.kostrych;

public class Flowerpot extends Product {
    double potCapacity;

    public Flowerpot(String nameOfProduct, int productPrice, double potCapacity) {
        super(nameOfProduct, productPrice);
        this.potCapacity = potCapacity;
    }

    public double getPotCapacity() {
        return potCapacity;
    }

    public void setPotCapacity(double potCapacity) {
        this.potCapacity = potCapacity;
    }
}
