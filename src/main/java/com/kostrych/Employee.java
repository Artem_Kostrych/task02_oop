package com.kostrych;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Employee{"
                + "name='"
                + name + '\''
                + '}';
    }

    public void closeShop(FlowerShop flowerShop) {
        flowerShop.setOpen(false);
    }

    public void openShop(FlowerShop flowerShop) {
        flowerShop.setOpen(true);
    }

    public Bouquet makeBouquet(FlowerShop shop) {
        List<Product> products = shop.getProducts();
        List<Flower> flowers = new LinkedList<Flower>();
        products = addFlower(flowers, products);
        String checking = new String();
        do {
            System.out.println("Do you want to add more" +
                    " flowers(Enter yes/no): ");
            Scanner sc = new Scanner(System.in);
            checking = sc.nextLine();
            if (checking.equals("no") || checking.equals("No")) {
                break;
            }
            products = this.addFlower(flowers, products);
        } while (checking.equals("yes") || checking.equals("Yes"));

        Bouquet bouquet = new Bouquet(flowers);
        return bouquet;
    }

    private List<Product> addFlower(List<Flower> flowers,
                                    List<Product> products) {
        System.out.println("Enter name of flower which you wanna add: ");
        List<Product> copyProducts = new LinkedList<Product>(products);
        Scanner sc = new Scanner(System.in);
        String nameOfFlowers;
        nameOfFlowers = sc.nextLine();
        for (Product prod : products) {
            if (prod.getNameOfProduct().equals(nameOfFlowers)) {
                flowers.add((Flower) prod);
                copyProducts.remove(prod);
                break;
            } else {
                if (products.indexOf(prod) == products.size() - 1) {
                    System.out.println("Can`t find this flower in the shop.");
                }
            }
        }
        return copyProducts;
    }

    public List<Flower> sortFlowers(Bouquet bouquet) {
        List<Flower> flowers = new LinkedList<Flower>(bouquet.getBouquet());
        for (int i = 0; i < bouquet.getBouquet().size(); i++) {
            for (int j = i + 1; j < bouquet.getBouquet().size(); j++) {
                if (flowers.get(i).getProductPrice() > flowers.get(j).getProductPrice()) {
                    Flower copyFlower = flowers.get(i);
                    flowers.set(i, flowers.get(j));
                    flowers.set(j, copyFlower);
                }
            }
        }
        return flowers;
    }

    public List<Product> findWithLambda(List<Product> productsList, List<Employee> employees) {
        Find b = ((List<Product> listOfProduct, String nameOfProduct) -> {
            List<Product> v = new LinkedList<Product>();
            for (Product prod : listOfProduct) {
                if (prod.getNameOfProduct().equals(nameOfProduct)) {
                    v.add(prod);
                }
            }
            return v;
        });

        return b.findByName(productsList, "Rose");
    }

}
