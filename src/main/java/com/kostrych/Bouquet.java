package com.kostrych;

import java.util.List;

public class Bouquet {
    private List<Flower> bouquet;

    public Bouquet(final List<Flower> bouquet) {
        this.bouquet = bouquet;
    }

    public final List<Flower> getBouquet() {
        return bouquet;
    }

    public final void setBouquet(final List<Flower> bouquet) {
        this.bouquet = bouquet;
    }

    @Override
    public final String toString() {
        return "Bouquet{"
                + "bouquet=" + bouquet
                + '}';
    }
}
